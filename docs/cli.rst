======================
Command Line Interface
======================

.. argparse::
   :module: ics2todoist
   :func: _build_argument_parser
   :prog: ics2todoist
