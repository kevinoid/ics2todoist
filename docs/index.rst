.. include:: ../README.rst
    :end-before: === End of Sphinx index content ===


Documentation Contents
======================

.. toctree::
   :maxdepth: 2

   cli
   api/ics2todoist
   contributing
   Issue Tracker <https://gitlab.com/kevinoid/ics2todoist/-/issues>
   License <https://gitlab.com/kevinoid/ics2todoist/-/raw/main/LICENSE.txt>


Indices and Tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. include:: ../README.rst
    :start-after: === Begin reference names ===
